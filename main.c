#include "header.h"
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h> 

int main(void) {
	int odabir_opcije;
	while (1) {
		system("cls");
		printf("|*********SKLADISTE*********|\n");
		printf("|___________________________|\n");
		printf("| 1. Dodavanje novih knjiga |\n");
		printf("|___________________________|\n");
		printf("| 2. Pretrazivanje knjiga   |\n");
		printf("|___________________________|\n");
		printf("|     3. Popis knjiga       |\n");
		printf("|___________________________|\n");
		printf("|   4. Uredivanje knjige    |\n");
		printf("|___________________________|\n");
		printf("|   5. Sortiranje knjiga    |\n");
		printf("|___________________________|\n");
		printf("|    6. Brisanje knjige     |\n");
		printf("|___________________________|\n");
		printf("|  7. Izlazak iz izbornika  |\n");
		printf("|___________________________|\n");
		printf("Izaberite opciju: ");

		odabir_opcije = getch();
		switch (odabir_opcije) {
		case '1': //otvara se funkcija nova_knjiga() s kojom se dodaje nova knjiga u skladi�te
			system("cls");
			if (nova_knjiga() == 1) {
				printf("\nGreska.\n");
			}
			printf("\n\nPritisnite bilo koju tipku za povratak...");
			getch();
			break;

		case '2': //otvara se novi izbornik koji nudi �etiri tipa pretra�ivanja knjige
			izbornik_pretrazivanje();
			printf("\n\nPritisnite bilo koju tipku za povratak...");
			getch();
			break;
		case '3': //ispisuju se sve knjige upisane u skladi�te
			system("cls");
			if (ispis_knjiga() == -1) {
				printf("\nGreska.\n");
			}
			printf("\n\nPritisnite bilo koju tipku za povratak...");
			getch();
			break;
		case '4':
			uredivanje_knjige();
			printf("\n\nPritisnite bilo koju tipku za povratak...");
			getch();
			break;
		case '5':
			system("cls");
			sortiranje_izbornik();
			break;
		case '6':
			brisanje_knjige();
			printf("\n\nPritisnite bilo koju tipku za povratak...");
			getch();
			break;
		case '7':
			system("cls");
			izlazak_iz_programa();
		default:
			/*printf("\nKrivi unos!\n");*/
			printf("\nPritisnite bilo koju tipku za povratak...");
			getch();

		}
	}
}

