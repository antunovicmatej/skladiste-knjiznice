#ifndef HEADER_H
#define HEADER_H

typedef struct djelo {
	char ime_djela[100];
	char ime_autora[50];
	int godina_izdanja;
	char vrsta_djela[50];
}DJELO;

int nova_knjiga();

void pretrazivanje(char);

void ispisKnjige(DJELO*);

int ispis_knjiga();

void izbornik_pretrazivanje();

void sortiranje_po_imenu_djela_silazno(DJELO*, int);

void sortiranje_po_imenu_djela_uzlazno(DJELO*, int);

void sortiranje_po_imenu_autora_silazno(DJELO*, int);

void sortiranje_po_imenu_autora_uzlazno(DJELO*, int);

void sortiranje_po_vrsti_djela_silazno(DJELO*, int);

void sortiranje_po_vrsti_djela_uzlazno(DJELO*, int);

void sortiranje_po_godini_silazno(DJELO*, int);

void sortiranje_po_godini_uzlazno(DJELO*, int);

void sortiranje_izbornik();

void brisanje_knjige();

void uredivanje_knjige();

void izlazak_iz_programa();

#endif