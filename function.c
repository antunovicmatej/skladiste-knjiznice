#include "header.h"
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>

int nova_knjiga() {
	FILE* fileP = NULL;
	DJELO* nova_knjiga = NULL;
	nova_knjiga = (DJELO*)malloc(sizeof(DJELO));
	int broj_knjiga;
	if (nova_knjiga == NULL) {
		return 1;
	}
	else {
		printf("Unesite ime djela: "); 
		scanf(" %99[^\n]", nova_knjiga->ime_djela);
		printf("Unesite ime autora: ");
		scanf(" %49[^\n]", nova_knjiga->ime_autora);
		printf("Unesite vrstu knjizevnog djela: ");
		scanf(" %49[^\n]", nova_knjiga->vrsta_djela);
		printf("Unesite godinu izdanja djela: ");
		scanf("%d", &nova_knjiga->godina_izdanja);
		fileP = fopen("knjige.bin", "rb"); 
		if (fileP == NULL) {
			fileP = fopen("knjige.bin", "wb");
			broj_knjiga = 1; 
			fwrite(&broj_knjiga, sizeof(int), 1, fileP);
			fwrite(nova_knjiga, sizeof(DJELO), 1, fileP);
			fclose(fileP);
		}
		else {
			fclose(fileP);
			fileP = fopen("knjige.bin", "rb+");
			if (fileP == NULL) { 
				return 1;
			}
			else {
				fseek(fileP, 0, SEEK_SET); 
				fread(&broj_knjiga, sizeof(int), 1, fileP);
				broj_knjiga++;
				fseek(fileP, 0, SEEK_SET); 
				fwrite(&broj_knjiga, sizeof(int), 1, fileP);
				fseek(fileP, 0, SEEK_END);
				fwrite(nova_knjiga, sizeof(DJELO), 1, fileP);
				fclose(fileP);
			}
		}
	}

	free(nova_knjiga);
	return 0;
}

void ispisKnjige(DJELO* knjige) { 
	printf("\n\nIme: %s", knjige->ime_djela);
	printf("\nAutor: %s", knjige->ime_autora);
	printf("\nVrsta knjizevnog djela: %s", knjige->vrsta_djela);
	printf("\nGodina izdanja: %d", knjige->godina_izdanja);

	return;
}

void pretrazivanje(char kriterij) { 
	DJELO* knjige = NULL;
	FILE* fileP = NULL;
	char trazeni_naslov[100], trazeni_autor[50], trazena_vrsta[50]; 
	int trazena_godina;

	int broj_knjiga, f = 0;
	system("cls");

	fileP = fopen("knjige.bin", "rb");
	if (fileP == NULL) {
		printf("\nGreska.\n"); 
	}
	else {
		fread(&broj_knjiga, sizeof(int), 1, fileP); 
		knjige = (DJELO*)malloc(sizeof(DJELO));
		if (knjige == NULL) {
			printf("Greska.\n");
		}
		else {
			switch (kriterij) {
			case 'i':
				printf("Unesite naslov trazene knjige: ");
				scanf(" %99[^\n]", trazeni_naslov);
				for (int i = 0; i < broj_knjiga; i++) {
					fread(knjige, sizeof(DJELO), 1, fileP);
					if (strcmp(knjige->ime_djela, trazeni_naslov) == 0) {
						printf("\nKnjiga je dostupna.");
						ispisKnjige(knjige);
						f = 1;
						break;
					}
				}
				break;

			case 'g': 
				printf("Unesite godinu trazene knjige: ");
				scanf(" %d", &trazena_godina);
				for (int i = 0; i < broj_knjiga; i++) {
					fread(knjige, sizeof(DJELO), 1, fileP);
					if (knjige->godina_izdanja == trazena_godina) {
						printf("\nKnjiga je dostupna.");
						ispisKnjige(knjige);
						f = 1;
					}
				}
				break;

			case 'a': 
				printf("Unesite autora trazene knjige: ");
				scanf(" %49[^\n]", trazeni_autor);
				for (int i = 0; i < broj_knjiga; i++) {
					fread(knjige, sizeof(DJELO), 1, fileP);
					if (strcmp(knjige->ime_autora, trazeni_autor) == 0) {
						printf("\nKnjiga je dostupna.");
						ispisKnjige(knjige);
						f = 1;
					}
				}
				break;

			case 'v': 
				printf("Unesite vrstu trazene knjige: ");
				scanf(" %49[^\n]", trazena_vrsta);
				for (int i = 0; i < broj_knjiga; i++) {
					fread(knjige, sizeof(DJELO), 1, fileP);
					if (strcmp(knjige->vrsta_djela, trazena_vrsta) == 0) {
						printf("\nKnjiga je dostupna.");
						ispisKnjige(knjige);
						f = 1;
					}
				}
				break;
			}
			if (f == 0) {
				printf("\nKnjiga nije dostupna");
			}
			fclose(fileP);
		}
	}
}

void brisanje_knjige() {
	FILE* fileP = NULL;
	DJELO* knjiga = NULL;
	int broj_knjiga;
	int index_za_brisanje;
	fileP = fopen("knjige.bin", "rb");
	if (fileP == NULL) {
		printf("\nGreska!");
	}
	else {
		fread(&broj_knjiga, sizeof(int), 1, fileP);
		knjiga = (DJELO*)malloc(broj_knjiga * sizeof(DJELO));

		if (knjiga == NULL) {
			printf("\nGreska!");
		}
		else {
			fread(knjiga, sizeof(DJELO), broj_knjiga, fileP);
			fclose(fileP);
			//pretraga
			char trazeni_naslov[100];
			int f = 0;
			system("cls");
			printf("Unesite naslov knjige koju zelite obrisati: ");
			scanf(" %99[^\n]", trazeni_naslov);
			for (int i = 0; i < broj_knjiga; i++) {
				if (strcmp((knjiga + i)->ime_djela, trazeni_naslov) == 0) {
					ispisKnjige(knjiga + i);
					index_za_brisanje = i;
					f = 1;
					break;
				}
			}
			if (f == 0) {
				printf("\nKnjiga nije pronadena");
			}
			else {
				fclose(fileP);
				fileP = fopen("knjige.bin", "wb");
				if (fileP == NULL) {
					printf("\nGreska!");
				}
				else {
					broj_knjiga--;
					fwrite(&broj_knjiga, sizeof(int), 1, fileP);
					for (int i = 0; i < broj_knjiga + 1; i++) {
						if (i == index_za_brisanje) {
							continue;
						}
						else {
							fwrite((knjiga + i), sizeof(DJELO), 1, fileP);
						}
					}
					printf("\n\nKnjiga je obrisana.");
					fclose(fileP);
				}
			}
		}
	}
}

void uredivanje_knjige() {
	FILE* fileP = NULL;
	DJELO* knjiga = NULL;
	int broj_knjiga;
	fileP = fopen("knjige.bin", "rb+");
	if (fileP == NULL) {
		printf("\nGreska!");
	}
	else {
		fread(&broj_knjiga, sizeof(int), 1, fileP);
		knjiga = (DJELO*)malloc(sizeof(DJELO));
		if (knjiga == NULL) {
			printf("\nGreska!");
		}
		else {
			//pretraga
			char trazeni_naslov[100];
			int f = 0;
			system("cls");
			printf("Unesite naslov knjige koje zelite urediti: ");
			scanf(" %99[^\n]", trazeni_naslov);
			for (int i = 0; i < broj_knjiga; i++) {
				fread(knjiga, sizeof(DJELO), 1, fileP);
				if (strcmp(knjiga->ime_djela, trazeni_naslov) == 0) {
					f = 1;
					ispisKnjige(knjiga);
						//izbornik 
						system("cls");
						printf("1. Promjena naslova");
						printf("\n2. Promjena autora");
						printf("\n3. Promjena vrste djela");
						printf("\n4. Promjena godine izdanja\n");
						printf("5. Povratak u glavni izbornik\n");
						char odabir = getch();
						switch (odabir) {
						case '1': // promjena naslova
							printf("\nUnesite novi naslov: ");
							char novi_naslov[100];
							scanf(" %99[^\n]", novi_naslov);
							strcpy((knjiga)->ime_djela, novi_naslov);
							fseek(fileP, -(int)sizeof(DJELO), SEEK_CUR);
							fwrite(knjiga, sizeof(DJELO), 1, fileP);
							printf("Naziv uspjesno promjenjen.\n");
							break;

						case '2': // promjena autora
							printf("\nUnesite novog autora: ");
							char novi_autor[50];
							scanf(" %49[^\n]", novi_autor);
							strcpy((knjiga)->ime_autora, novi_autor);
							fseek(fileP, -(int)sizeof(DJELO), SEEK_CUR);
							fwrite(knjiga, sizeof(DJELO), 1, fileP);
							printf("\nAutor uspjesno promjenjen.\n");
							break;

						case '3': // promjena vrste
							printf("\nUnesite novu vrstu djela: ");
							char nova_vrsta[50];
							scanf(" %49[^\n]", nova_vrsta);
							strcpy((knjiga)->vrsta_djela, nova_vrsta);
							fseek(fileP, -(int)sizeof(DJELO), SEEK_CUR);
							fwrite(knjiga, sizeof(DJELO), 1, fileP);
							printf("\nVrsta uspjesno promjenjena.\n");
							break;

						case '4': // promjena godine
							printf("\nUnesite novu godinu: ");
							int nova_godina;
							scanf("%d", &nova_godina);
							knjiga->godina_izdanja = nova_godina;
							fseek(fileP, -(int)sizeof(DJELO), SEEK_CUR);
							fwrite(knjiga, sizeof(DJELO), 1, fileP);
							printf("\nGodina uspjesno promjenjena.\n");
							break;

						case '5':
							return;
							break;
						default:
							printf("Krivi unos.");
						}
						break;
				}
			}

			if (f == 0) {
				printf("\nKnjiga nije pronadena");
			}
		}
		fclose(fileP);
	}
	free(knjiga);
	knjiga = NULL;
}

void izbornik_pretrazivanje() { 
	system("cls");
	printf("1. Pretrazivanje po imenu\n");
	printf("2. Pretrazivanje po autoru\n");
	printf("3. Pretrazivanje po vrsti\n");
	printf("4. Pretrazivanje po godini\n");
	printf("5. Povratak u glavni izbornik\n");
	char odabir = getch();
	switch (odabir) {
	case '1':
		pretrazivanje('i'); 
		break;

	case '2':
		pretrazivanje('a');
		break;

	case '3':
		pretrazivanje('v');
		break;

	case '4':
		pretrazivanje('g');
		break;

	case '5':
		return;
		break;
	default:
		printf("\nKrivi unos.");
	}
}

void sortiranje_po_imenu_djela_silazno(DJELO* knjige, int broj_knjiga) {
	DJELO temp;
	char prvo_slovo, prvo_slovo_pom;
	int min;


	for (int i = 0; i < broj_knjiga - 1; i++) {
		min = i;
		prvo_slovo = (knjige + i)->ime_djela[0] >= 'A' && (knjige + i)->ime_djela[0] <= 'Z' ? (knjige + i)->ime_djela[0] : (knjige + i)->ime_djela[0] - 32;
		for (int j = i + 1; j < broj_knjiga; j++) {
			prvo_slovo_pom = (knjige + j)->ime_djela[0] >= 'A' && (knjige + j)->ime_djela[0] <= 'Z' ? (knjige + j)->ime_djela[0] : (knjige + j)->ime_djela[0] - 32;
			if (prvo_slovo < prvo_slovo_pom) {
				min = j;
				prvo_slovo = (knjige + j)->ime_djela[0] >= 'A' && (knjige + j)->ime_djela[0] <= 'Z' ? (knjige + j)->ime_djela[0] : (knjige + j)->ime_djela[0] - 32;
			}
		}
		//zamjena imena djela
		strcpy(temp.ime_djela, (knjige + i)->ime_djela);
		strcpy((knjige + i)->ime_djela, (knjige + min)->ime_djela);
		strcpy((knjige + min)->ime_djela, temp.ime_djela);
		//zamjena autora djela
		strcpy(temp.ime_autora, (knjige + i)->ime_autora);
		strcpy((knjige + i)->ime_autora, (knjige + min)->ime_autora);
		strcpy((knjige + min)->ime_autora, temp.ime_autora);
		//zamjena vrste djela
		strcpy(temp.vrsta_djela, (knjige + i)->vrsta_djela);
		strcpy((knjige + i)->vrsta_djela, (knjige + min)->vrsta_djela);
		strcpy((knjige + min)->vrsta_djela, temp.vrsta_djela);
		//zamjena godine izdanja
		temp.godina_izdanja = (knjige + i)->godina_izdanja;
		(knjige + i)->godina_izdanja = (knjige + min)->godina_izdanja;
		(knjige + min)->godina_izdanja = temp.godina_izdanja;
	}

	return;
}

void sortiranje_po_imenu_djela_uzlazno(DJELO* knjige, int broj_knjiga) {
	DJELO temp;
	char prvo_slovo, prvo_slovo_pom;
	int max;

	for (int i = 0; i < broj_knjiga - 1; i++) {
		max = i;
		prvo_slovo = (knjige + i)->ime_djela[0] >= 'A' && (knjige + i)->ime_djela[0] <= 'Z' ? (knjige + i)->ime_djela[0] : (knjige + i)->ime_djela[0] - 32;
		for (int j = i + 1; j < broj_knjiga; j++) {
			prvo_slovo_pom = (knjige + j)->ime_djela[0] >= 'A' && (knjige + j)->ime_djela[0] <= 'Z' ? (knjige + j)->ime_djela[0] : (knjige + j)->ime_djela[0] - 32;
			if (prvo_slovo > prvo_slovo_pom) {
				max = j;
				prvo_slovo = (knjige + j)->ime_djela[0] >= 'A' && (knjige + j)->ime_djela[0] <= 'Z' ? (knjige + j)->ime_djela[0] : (knjige + j)->ime_djela[0] - 32;
			}
		}
		//zamjena imena djela
		strcpy(temp.ime_djela, (knjige + i)->ime_djela);
		strcpy((knjige + i)->ime_djela, (knjige + max)->ime_djela);
		strcpy((knjige + max)->ime_djela, temp.ime_djela);
		//zamjena autora djela
		strcpy(temp.ime_autora, (knjige + i)->ime_autora);
		strcpy((knjige + i)->ime_autora, (knjige + max)->ime_autora);
		strcpy((knjige + max)->ime_autora, temp.ime_autora);
		//zamjena vrste djela
		strcpy(temp.vrsta_djela, (knjige + i)->vrsta_djela);
		strcpy((knjige + i)->vrsta_djela, (knjige + max)->vrsta_djela);
		strcpy((knjige + max)->vrsta_djela, temp.vrsta_djela);
		//zamjena godine izdanja
		temp.godina_izdanja = (knjige + i)->godina_izdanja;
		(knjige + i)->godina_izdanja = (knjige + max)->godina_izdanja;
		(knjige + max)->godina_izdanja = temp.godina_izdanja;
	}

	return;
}

void sortiranje_po_imenu_autora_silazno(DJELO* knjige, int broj_knjiga) {
	DJELO temp;
	char prvo_slovo, prvo_slovo_pom;
	int min;

	for (int i = 0; i < broj_knjiga - 1; i++) {
		min = i;
		prvo_slovo = (knjige + i)->ime_autora[0] >= 'A' && (knjige + i)->ime_autora[0] <= 'Z' ? (knjige + i)->ime_autora[0] : (knjige + i)->ime_autora[0] - 32;
		for (int j = i + 1; j < broj_knjiga; j++) {
			prvo_slovo_pom = (knjige + j)->ime_autora[0] >= 'A' && (knjige + j)->ime_autora[0] <= 'Z' ? (knjige + j)->ime_autora[0] : (knjige + j)->ime_autora[0] - 32;
			if (prvo_slovo < prvo_slovo_pom) {
				min = j;
				prvo_slovo = (knjige + j)->ime_autora[0] >= 'A' && (knjige + j)->ime_autora[0] <= 'Z' ? (knjige + j)->ime_autora[0] : (knjige + j)->ime_autora[0] - 32;
			}
		}
		//zamjena imena djela
		strcpy(temp.ime_djela, (knjige + i)->ime_djela);
		strcpy((knjige + i)->ime_djela, (knjige + min)->ime_djela);
		strcpy((knjige + min)->ime_djela, temp.ime_djela);
		//zamjena autora djela
		strcpy(temp.ime_autora, (knjige + i)->ime_autora);
		strcpy((knjige + i)->ime_autora, (knjige + min)->ime_autora);
		strcpy((knjige + min)->ime_autora, temp.ime_autora);
		//zamjena vrste djela
		strcpy(temp.vrsta_djela, (knjige + i)->vrsta_djela);
		strcpy((knjige + i)->vrsta_djela, (knjige + min)->vrsta_djela);
		strcpy((knjige + min)->vrsta_djela, temp.vrsta_djela);
		//zamjena godine izdanja
		temp.godina_izdanja = (knjige + i)->godina_izdanja;
		(knjige + i)->godina_izdanja = (knjige + min)->godina_izdanja;
		(knjige + min)->godina_izdanja = temp.godina_izdanja;
	}

	return;
}

void sortiranje_po_imenu_autora_uzlazno(DJELO* knjige, int broj_knjiga) {
	DJELO temp;
	char prvo_slovo, prvo_slovo_pom;
	int max;

	for (int i = 0; i < broj_knjiga - 1; i++) {
		max = i;
		prvo_slovo = (knjige + i)->ime_autora[0] >= 'A' && (knjige + i)->ime_autora[0] <= 'Z' ? (knjige + i)->ime_autora[0] : (knjige + i)->ime_autora[0] - 32;
		for (int j = i + 1; j < broj_knjiga; j++) {
			prvo_slovo_pom = (knjige + j)->ime_autora[0] >= 'A' && (knjige + j)->ime_autora[0] <= 'Z' ? (knjige + j)->ime_autora[0] : (knjige + j)->ime_autora[0] - 32;
			if (prvo_slovo > prvo_slovo_pom) {
				max = j;
				prvo_slovo = (knjige + j)->ime_autora[0] >= 'A' && (knjige + j)->ime_autora[0] <= 'Z' ? (knjige + j)->ime_autora[0] : (knjige + j)->ime_autora[0] - 32;
			}
		}
		//zamjena imena djela
		strcpy(temp.ime_djela, (knjige + i)->ime_djela);
		strcpy((knjige + i)->ime_djela, (knjige + max)->ime_djela);
		strcpy((knjige + max)->ime_djela, temp.ime_djela);
		//zamjena autora djela
		strcpy(temp.ime_autora, (knjige + i)->ime_autora);
		strcpy((knjige + i)->ime_autora, (knjige + max)->ime_autora);
		strcpy((knjige + max)->ime_autora, temp.ime_autora);
		//zamjena vrste djela
		strcpy(temp.vrsta_djela, (knjige + i)->vrsta_djela);
		strcpy((knjige + i)->vrsta_djela, (knjige + max)->vrsta_djela);
		strcpy((knjige + max)->vrsta_djela, temp.vrsta_djela);
		//zamjena godine izdanja
		temp.godina_izdanja = (knjige + i)->godina_izdanja;
		(knjige + i)->godina_izdanja = (knjige + max)->godina_izdanja;
		(knjige + max)->godina_izdanja = temp.godina_izdanja;
	}

	return;
}

void sortiranje_po_vrsti_djela_silazno(DJELO* knjige, int broj_knjiga) {
	DJELO temp;
	char prvo_slovo, prvo_slovo_pom;
	int min;


	for (int i = 0; i < broj_knjiga - 1; i++) {
		min = i;
		prvo_slovo = (knjige + i)->vrsta_djela[0] >= 'A' && (knjige + i)->vrsta_djela[0] <= 'Z' ? (knjige + i)->vrsta_djela[0] : (knjige + i)->vrsta_djela[0] - 32;
		for (int j = i + 1; j < broj_knjiga; j++) {
			prvo_slovo_pom = (knjige + j)->vrsta_djela[0] >= 'A' && (knjige + j)->vrsta_djela[0] <= 'Z' ? (knjige + j)->vrsta_djela[0] : (knjige + j)->vrsta_djela[0] - 32;
			if (prvo_slovo < prvo_slovo_pom) {
				min = j;
				prvo_slovo = (knjige + j)->vrsta_djela[0] >= 'A' && (knjige + j)->vrsta_djela[0] <= 'Z' ? (knjige + j)->vrsta_djela[0] : (knjige + j)->vrsta_djela[0] - 32;
			}
		}
		//zamjena imena djela
		strcpy(temp.ime_djela, (knjige + i)->ime_djela);
		strcpy((knjige + i)->ime_djela, (knjige + min)->ime_djela);
		strcpy((knjige + min)->ime_djela, temp.ime_djela);
		//zamjena autora djela
		strcpy(temp.ime_autora, (knjige + i)->ime_autora);
		strcpy((knjige + i)->ime_autora, (knjige + min)->ime_autora);
		strcpy((knjige + min)->ime_autora, temp.ime_autora);
		//zamjena vrste djela
		strcpy(temp.vrsta_djela, (knjige + i)->vrsta_djela);
		strcpy((knjige + i)->vrsta_djela, (knjige + min)->vrsta_djela);
		strcpy((knjige + min)->vrsta_djela, temp.vrsta_djela);
		//zamjena godine izdanja
		temp.godina_izdanja = (knjige + i)->godina_izdanja;
		(knjige + i)->godina_izdanja = (knjige + min)->godina_izdanja;
		(knjige + min)->godina_izdanja = temp.godina_izdanja;
	}

	return;
}

void sortiranje_po_vrsti_djela_uzlazno(DJELO* knjige, int broj_knjiga) {
	DJELO temp;
	char prvo_slovo, prvo_slovo_pom;
	int max;

	for (int i = 0; i < broj_knjiga - 1; i++) {
		max = i;
		prvo_slovo = (knjige + i)->vrsta_djela[0] >= 'A' && (knjige + i)->vrsta_djela[0] <= 'Z' ? (knjige + i)->vrsta_djela[0] : (knjige + i)->vrsta_djela[0] - 32;
		for (int j = i + 1; j < broj_knjiga; j++) {
			prvo_slovo_pom = (knjige + j)->vrsta_djela[0] >= 'A' && (knjige + j)->vrsta_djela[0] <= 'Z' ? (knjige + j)->vrsta_djela[0] : (knjige + j)->vrsta_djela[0] - 32;
			if (prvo_slovo > prvo_slovo_pom) {
				max = j;
				prvo_slovo = (knjige + j)->vrsta_djela[0] >= 'A' && (knjige + j)->vrsta_djela[0] <= 'Z' ? (knjige + j)->vrsta_djela[0] : (knjige + j)->vrsta_djela[0] - 32;
			}
		}
		//zamjena imena djela
		strcpy(temp.ime_djela, (knjige + i)->ime_djela);
		strcpy((knjige + i)->ime_djela, (knjige + max)->ime_djela);
		strcpy((knjige + max)->ime_djela, temp.ime_djela);
		//zamjena autora djela
		strcpy(temp.ime_autora, (knjige + i)->ime_autora);
		strcpy((knjige + i)->ime_autora, (knjige + max)->ime_autora);
		strcpy((knjige + max)->ime_autora, temp.ime_autora);
		//zamjena vrste djela
		strcpy(temp.vrsta_djela, (knjige + i)->vrsta_djela);
		strcpy((knjige + i)->vrsta_djela, (knjige + max)->vrsta_djela);
		strcpy((knjige + max)->vrsta_djela, temp.vrsta_djela);
		//zamjena godine izdanja
		temp.godina_izdanja = (knjige + i)->godina_izdanja;
		(knjige + i)->godina_izdanja = (knjige + max)->godina_izdanja;
		(knjige + max)->godina_izdanja = temp.godina_izdanja;
	}

	return;
}

void sortiranje_po_godini_uzlazno(DJELO* knjige, int broj_knjiga) {
	DJELO temp;
	int godina1, godina2;
	int min;

	for (int i = 0; i < broj_knjiga - 1; i++) {
		min = i;
		godina1 = (knjige + i)->godina_izdanja;
		for (int j = i + 1; j < broj_knjiga; j++) {
			godina2 = (knjige + j)->godina_izdanja;
			if (godina1 > godina2) {
				min = j;
				godina1 = (knjige + j)->godina_izdanja;
			}
		}
		//zamjena imena djela
		strcpy(temp.ime_djela, (knjige + i)->ime_djela);
		strcpy((knjige + i)->ime_djela, (knjige + min)->ime_djela);
		strcpy((knjige + min)->ime_djela, temp.ime_djela);
		//zamjena autora djela
		strcpy(temp.ime_autora, (knjige + i)->ime_autora);
		strcpy((knjige + i)->ime_autora, (knjige + min)->ime_autora);
		strcpy((knjige + min)->ime_autora, temp.ime_autora);
		//zamjena vrste djela
		strcpy(temp.vrsta_djela, (knjige + i)->vrsta_djela);
		strcpy((knjige + i)->vrsta_djela, (knjige + min)->vrsta_djela);
		strcpy((knjige + min)->vrsta_djela, temp.vrsta_djela);
		//zamjena godine izdanja
		temp.godina_izdanja = (knjige + i)->godina_izdanja;
		(knjige + i)->godina_izdanja = (knjige + min)->godina_izdanja;
		(knjige + min)->godina_izdanja = temp.godina_izdanja;
	}

	return;
}

void sortiranje_po_godini_silazno(DJELO* knjige, int broj_knjiga) {
	DJELO temp;
	int godina1, godina2;
	int max;

	for (int i = 0; i < broj_knjiga - 1; i++) {
		max = i;
		godina1 = (knjige + i)->godina_izdanja;
		for (int j = i + 1; j < broj_knjiga; j++) {
			godina2 = (knjige + j)->godina_izdanja;
			if (godina1 < godina2) {
				max = j;
				godina1 = (knjige + j)->godina_izdanja;
			}
		}
		//zamjena imena djela
		strcpy(temp.ime_djela, (knjige + i)->ime_djela);
		strcpy((knjige + i)->ime_djela, (knjige + max)->ime_djela);
		strcpy((knjige + max)->ime_djela, temp.ime_djela);
		//zamjena autora djela
		strcpy(temp.ime_autora, (knjige + i)->ime_autora);
		strcpy((knjige + i)->ime_autora, (knjige + max)->ime_autora);
		strcpy((knjige + max)->ime_autora, temp.ime_autora);
		//zamjena vrste djela
		strcpy(temp.vrsta_djela, (knjige + i)->vrsta_djela);
		strcpy((knjige + i)->vrsta_djela, (knjige + max)->vrsta_djela);
		strcpy((knjige + max)->vrsta_djela, temp.vrsta_djela);
		//zamjena godine izdanja
		temp.godina_izdanja = (knjige + i)->godina_izdanja;
		(knjige + i)->godina_izdanja = (knjige + max)->godina_izdanja;
		(knjige + max)->godina_izdanja = temp.godina_izdanja;
	}

	return;
}

void sortiranje_izbornik() {
	DJELO* knjige = NULL;
	FILE* fileP = NULL;
	char prvi_odabir, drugi_odabir;
	char odabir[30];

	int broj_knjiga;
	system("cls");

	fileP = fopen("knjige.bin", "rb"); //otvaranje datoteke u modu za citanje
	if (fileP == NULL) {
		printf("\nGreska.\n"); //ispisivanje gre�ke ako se datoteka nije uspje�no otvorila
	}
	else {
		fread(&broj_knjiga, sizeof(int), 1, fileP); //citanje broja knjiga
		knjige = (DJELO*)malloc(broj_knjiga * sizeof(DJELO));
		if (knjige == NULL) {
			printf("\nGreska!");
		}
		else {
			fread(knjige, sizeof(DJELO), broj_knjiga, fileP);
			system("cls");
			printf("Odaberite nacin sortiranja:\n");
			printf("1. Ime knjige\n2. Ime autora\n3. Vrsta djela\n4. Godina izdanja\n");
			do {
				prvi_odabir = getch();
			} while (prvi_odabir < '1' || prvi_odabir > '4');
			printf("Redoslijed:\n1. Uzlazno\n2. Silazno");
			do {
				drugi_odabir = getch();
			} while (drugi_odabir < '1' || drugi_odabir > '2');

			switch (prvi_odabir) {
			case '1':
				if (drugi_odabir == '1') {
					sortiranje_po_imenu_djela_uzlazno(knjige, broj_knjiga);
				}
				else {
					sortiranje_po_imenu_djela_silazno(knjige, broj_knjiga);
				}
				break;
			case '2':
				if (drugi_odabir == '1') {
					sortiranje_po_imenu_autora_uzlazno(knjige, broj_knjiga);
				}
				else {
					sortiranje_po_imenu_autora_silazno(knjige, broj_knjiga);
				}
				break;
			case '3':
				if (drugi_odabir == '1') {
					sortiranje_po_vrsti_djela_uzlazno(knjige, broj_knjiga);
				}
				else {
					sortiranje_po_vrsti_djela_silazno(knjige, broj_knjiga);
				}
				break;
			case '4':
				if (drugi_odabir == '1') {
					sortiranje_po_godini_uzlazno(knjige, broj_knjiga);
				}
				else {
					sortiranje_po_godini_silazno(knjige, broj_knjiga);
				}
				break;
			}
			fclose(fileP);
			fileP = fopen("knjige.bin", "wb");
			if (fileP == NULL) {
				printf("\nGreska!");
			}
			else {
				//azuriranje datoteke
				fwrite(&broj_knjiga, sizeof(int), 1, fileP);
				fwrite(knjige, sizeof(DJELO), broj_knjiga, fileP);
				fclose(fileP);
			}
			free(knjige);
			knjige = NULL;
		}
	}
	printf("\n\nSortiranje uspjesno obavljeno.");
	printf("\nPritisnite bilo koju tipku za povratak...");
	getch();
}

int ispis_knjiga() {
	int broj;
	DJELO* knjige = NULL;
	FILE* fileP = NULL;
	fileP = fopen("knjige.bin", "rb");
	if (fileP == NULL) {
		return 1;
	}
	else {
		fread(&broj, sizeof(int), 1, fileP);
		knjige = (DJELO*)malloc(sizeof(DJELO));
		if (knjige == NULL) {
			return 1;
		}
		else {
			printf("U knjiznici ima %d knjig%s.", broj, broj == 1 || broj > 4 ? "a" : "e");
			for (int i = 0; i < broj; i++) {
				fread(knjige, sizeof(DJELO), 1, fileP);
				ispisKnjige(knjige);
			}
			free(knjige);
			fclose(fileP);
			return broj;
		}
	}
}

void izlazak_iz_programa() {
	char odabir[3];
	printf("Zelite li izaci iz programa? (da/ne) ");
	scanf(" %2s", odabir);
	if (strcmp(odabir, "da") == 0 || strcmp(odabir, "Da") == 0 || strcmp(odabir, "DA") == 0 || strcmp(odabir, "dA") == 0) { //uspore�ivanje unesenog stringa 
		printf("\nIzlazak iz programa...");
		exit(EXIT_SUCCESS);
	}
	else if (strcmp(odabir, "ne") == 0 || strcmp(odabir, "Ne") == 0 || strcmp(odabir, "NE") == 0 || strcmp(odabir, "nE") == 0) {
		return;
	}
}